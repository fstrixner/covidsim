import asyncio
import zmq.asyncio
from time import time_ns
import pandas as pd
import multiprocessing

from default_parameters import DEFAULT_PARAMETERS

NUM_CALLS = 1000 # number of calls to the simulator, to create some load for throughput testing
NUM_PARALLEL_REQUESTS = multiprocessing.cpu_count() - 2 # max number of parallel requests - so we don't overload the request queues for large NUM_CALLS
print(f'CPU core count: {NUM_PARALLEL_REQUESTS}')
SIMULATION_URL = "tcp://127.0.0.1"
SIMULATION_BASEPORT = 3000

request_queue = asyncio.Queue()

class Connection:
    def __init__(self, port, interface='tcp://127.0.0.1'):
        _ZMQ_PAIR = 0
        # _ZMQ_LINGER = 17
        self._context = context = zmq.asyncio.Context()
        self.socket = self._context.socket(_ZMQ_PAIR)
        # self.sendSocket.setsockopt(_ZMQ_LINGER, 0)
        url = f"{interface}:{port}"
        print(f"connecting to {url}")
        self.socket.connect(url)

    def __del__(self):
        self.socket.close()
        self._context.destroy()

async def dequeue_params_and_simulate(workerId):
  connection = Connection(SIMULATION_BASEPORT + workerId, SIMULATION_URL)
  results = []
  while not request_queue.empty():
    params = await request_queue.get()
    results.append(await covidsim_simulate(params, connection))
  return results


async def covidsim_simulate(parameter, connection):
  await connection.socket.send_json(parameter)
  response = await connection.socket.recv_json()
  return response


def parse_result(json_result):
    # NOTE(mrksr): The response JSON contains one key per timeseries. We parse
    # them in turn and concat them to a single dataframe.
    def parse_single_timeseries(timeseries, key):
      df = pd.DataFrame(timeseries["values"])
      df = df.rename(columns={"x": "days", "y": key})
      df = df.set_index("days")
      return df

    return pd.concat(
      [
        parse_single_timeseries(timeseries, key)
        for key, timeseries in json_result.items()
        if isinstance(timeseries, dict)
      ],
      axis=1,
    )


async def main():
  for i in range(0, NUM_CALLS):
    await request_queue.put(DEFAULT_PARAMETERS)  # usually one would enqueue different parameter sets...

  tasks = []
  start_time = time_ns()
  for workerId in range(0, NUM_PARALLEL_REQUESTS):
    tasks.append(dequeue_params_and_simulate(workerId))
  responses = [item for sublist in await asyncio.gather(*tasks) for item in sublist]
  end_time = time_ns()
  print(f'took: {(end_time - start_time) / 1000000000} s\n')
  print(responses[0])
  print(parse_result(responses[0]))

asyncio.run(main())
