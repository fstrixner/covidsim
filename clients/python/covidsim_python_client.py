import requests
import json
import pandas as pd
from default_parameters import DEFAULT_PARAMETERS

SIMULATION_URL = "http://localhost:3000"


def covidsim_simulate(parameter, url=SIMULATION_URL):
  headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
  r = requests.post(url, data=json.dumps(parameter), headers=headers)
  return r.json()


def parse_result(json_result):
    # NOTE(mrksr): The response JSON contains one key per timeseries. We parse
    # them in turn and concat them to a single dataframe.
    def parse_single_timeseries(timeseries, key):
      df = pd.DataFrame(timeseries["values"])
      df = df.rename(columns={"x": "days", "y": key})
      df = df.set_index("days")
      return df

    return pd.concat(
      [
        parse_single_timeseries(timeseries, key)
        for key, timeseries in json_result.items()
        if isinstance(timeseries, dict)
      ],
      axis=1,
    )


response = covidsim_simulate(DEFAULT_PARAMETERS)
print(response)
print(parse_result(response))
