import asyncio
import aiohttp as aiohttp
from time import time_ns
import json
import pandas as pd

from default_parameters import DEFAULT_PARAMETERS

NUM_CALLS = 1000 # number of calls to the simulator, to create some load for throughput testing
NUM_PARALLEL_REQUESTS = 50 # max number of parallel requests - so we don't overload the request queues for large NUM_CALLS
SIMULATION_URL = "http://localhost:3000"

request_queue = asyncio.Queue()


async def dequeue_params_and_simulate():
  results = []
  while not request_queue.empty():
    params = await request_queue.get()
    results.append(await covidsim_simulate(params))
  return results


async def covidsim_simulate(parameter, url=SIMULATION_URL):
  headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
  async with aiohttp.ClientSession() as session:
    async with session.post(url, data=json.dumps(parameter), headers=headers) as response:
      return await response.json()

def parse_result(json_result):
    # NOTE(mrksr): The response JSON contains one key per timeseries. We parse
    # them in turn and concat them to a single dataframe.
    def parse_single_timeseries(timeseries, key):
      df = pd.DataFrame(timeseries["values"])
      df = df.rename(columns={"x": "days", "y": key})
      df = df.set_index("days")
      return df

    return pd.concat(
      [
        parse_single_timeseries(timeseries, key)
        for key, timeseries in json_result.items()
        if isinstance(timeseries, dict)
      ],
      axis=1,
    )


async def main():
  for i in range(0, NUM_CALLS):
    await request_queue.put(DEFAULT_PARAMETERS)  # usually one would enqueue different parameter sets...

  tasks = []
  start_time = time_ns()
  for _ in range(0, NUM_PARALLEL_REQUESTS):
    tasks.append(dequeue_params_and_simulate())
  responses = [item for sublist in await asyncio.gather(*tasks) for item in sublist]
  end_time = time_ns()
  print(f'took: {(end_time - start_time) / 1000000000} s\n')
  print(responses[0])
  print(parse_result(responses[0]))

asyncio.run(main())
