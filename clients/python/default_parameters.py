DEFAULT_PARAMETERS = {
    # Population
    "population_size": 100.0,
    "initial_cases": 1.0,
    "external_force_of_infection": 1.0,
    "simulation_days": 365.0,
    # Durations
    "latency_duration": 4.0,
    "prodromal_duration": 1.0,
    "infective_duration": 10.0,
    "declining_duration": 0.0,
    "latency_stages": 16.0,
    "prodromal_stages": 16.0,
    "infective_stages": 16.0,
    "declining_stages": 0.0,
    "hospitalisation_duration": 14.0,
    "icu_duration": 14.0,
    # "erlang_stages": 16.0,
    # Severity
    "symptomatic_course_of_disease": 58.0,
    "medical_consultations_by_symptomatic_cases": 40.0,
    "hospitalisation_of_symptomatic_cases": 2.0,
    "icu_patients_among_hospitalized_cases": 25.0,
    "case_fatality_of_symptomatic_cases": 2.0,
    # Contagiousness
    "basic_reproduction_number": 4.0,
    "maximal_seasonal_transmission_factor": 0.0,
    "maximal_seasonal_transmission_day": 0.0,
    "prodromal_contagiousness": 50.0,
    "declining_contagiousness": 100.0,
    # Case Isolation
    "symptomatic_cases_to_be_isolated": 0.0,
    "case_isolation_capacity": 0.0,
    "contact_reduction_home_isolation": 0.0,
    "case_isolation_begin": 0.0,
    "case_isolation_range": 0.0,
    # General Contact Reduction
    "contact_reduction_factor": 0.0,
    "contact_reduction_begin": 0.0,
    "contact_reduction_range": 0.0,
    # Triggered General Contact Reduction
    "sick_threshold": 0.0,
    "sick_triggered_contact_reduction": 0.0,
    "hospitalisation_threshold": 0.0,
    "hospitalisation_triggered_contact_reduction": 0.0,
    "icu_threshold": 0.0,
    "icu_triggered_contact_reduction": 0.0,
    # Detection
    "detection_probability_medical_consultation": 10.0,
    "detection_probability_hospitalisation": 10.0,
    "detection_probability_deceased_case": 10.0,
}
