import { Component } from '@angular/core';
import { AppstateService } from './services/appstate.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  side$: Observable<boolean>;

  constructor(
    appstateService: AppstateService
  ) {
    this.side$ = appstateService.of('Side');
  }

}

