import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng5SliderModule } from 'ng5-slider';
import { ChartsModule } from 'ng2-charts';

import { NvD3Module } from 'ng2-nvd3';

import { AppComponent } from './app.component';
import { MaterialModule } from './shared/material.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ControlComponent } from './components/control/control.component';
import { ContentComponent } from './components/content/content.component';
import { SliderComponent } from './components/input/slider/slider.component';
import { AboutComponent } from './components/dialogs/about/about.component';
import { HelpComponent } from './components/dialogs/help/help.component';
import { ImpressumComponent } from './components/dialogs/impressum/impressum.component';

import 'd3';
import 'nvd3';
import { SensitivityComponent } from './components/sensitivity/sensitivity.component';
import { SideToggleComponent } from './components/side-toggle.component';
import { InputComponent } from './components/input/input.component';
import { OverviewChartComponent } from './components/charts/overview-chart.component';
import { PrevalenceChartComponent } from './components/charts/prevalence-chart.component';
import { IncidenceChartComponent } from './components/charts/incidence-chart.component';
import { SensitivityChartComponent } from './components/charts/sensitivity-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ControlComponent,
    ContentComponent,
    SliderComponent,
    HelpComponent,
    AboutComponent,
    ImpressumComponent,
    SensitivityComponent,
    SideToggleComponent,
    InputComponent,
    OverviewChartComponent,
    PrevalenceChartComponent,
    IncidenceChartComponent,
    SensitivityChartComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MaterialModule,
    Ng5SliderModule,
    ChartsModule,
    NvD3Module,
  ],
  providers: [],
  entryComponents: [
    AboutComponent,
    HelpComponent,
    ImpressumComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
