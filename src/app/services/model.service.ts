import { Injectable, OnDestroy } from '@angular/core';
import {Observable, Subject, BehaviorSubject, ObservableInput, zip, of} from 'rxjs';
// import { fromWorkerPool } from 'observable-webworker';
import { saveAs } from 'file-saver';


import { ParameterSettings } from '../model/parameter-settings';
import { SimulationRequest } from '../model/simulation-request';
import { PARAMETER_PROPERTIES } from '../model/parameter-properties';
import { SimulationResult } from '../model/simulation-result';
import {fromWorker, WorkerPoolOptions} from 'observable-webworker';
import {finalize, map, mergeAll, tap} from 'rxjs/operators';

interface LazyWorker {
  factory: () => Worker;
  terminate: () => void;
  processing: boolean;
  started: boolean;
  index: number;
}

export function fromWorkerPool<I, O>(
    workerConstructor: (index: number) => Worker,
    workUnitIterator: ObservableInput<I>,
    options?: WorkerPoolOptions<I, O>,
): Observable<O> {
  const {
    // tslint:disable-next-line:no-unnecessary-initializer
    selectTransferables = undefined,
    workerCount = 4, // navigator.hardwareConcurrency ? navigator.hardwareConcurrency - 1 : null,
    fallbackWorkerCount = 3,
    flattenOperator = mergeAll<O>(),
  } = options || {};

  return new Observable<O>(resultObserver => {
    const idleWorker$$: Subject<LazyWorker> = new Subject();

    let completed = 0;
    let sent = 0;
    let finished = false;

    const lazyWorkers: LazyWorker[] = Array.from({
      length: workerCount !== null ? workerCount : fallbackWorkerCount,
    }).map((_, index) => {
      return {
        _cachedWorker: null,
        factory() {
          if (!this._cachedWorker) {
            this._cachedWorker = workerConstructor(index);
            this.started = true;
          }
          return this._cachedWorker;
        },
        terminate() {
          if (this.started && !this.processing) {
            this._cachedWorker.terminate();
          }
        },
        processing: false,
        started: false,
        index,
      };
    });

    const processor$ = zip(idleWorker$$, workUnitIterator).pipe(
        tap(([worker]) => {
          sent++;
          worker.processing = true;
        }),
        finalize(() => {
          idleWorker$$.complete();
          finished = true;
          lazyWorkers.forEach(worker => worker.terminate());
        }),
        map(
            ([worker, unitWork]): Observable<O> => {
              return fromWorker<I, O>(() => worker.factory(), of(unitWork), selectTransferables, {
                terminateOnComplete: false,
              }).pipe(
                  finalize(() => {
                    completed++;

                    worker.processing = false;

                    if (!finished) {
                      idleWorker$$.next(worker);
                    } else {
                      worker.terminate();
                    }

                    if (finished && completed === sent) {
                      resultObserver.complete();
                    }
                  }),
              );
            },
        ),
        flattenOperator,
    );

    const sub = processor$.subscribe(resultObserver);

    lazyWorkers.forEach(w => idleWorker$$.next(w));

    return () => sub.unsubscribe();
  });
}

@Injectable({
  providedIn: 'root'
})
export class ModelService implements OnDestroy {

  dataset = new BehaviorSubject<SimulationResult>(null);

  parameters: ParameterSettings;
  simulationRequest = new Subject<SimulationRequest>();

  constructor() {
    fromWorkerPool<any, any>( () =>
    new Worker('../model/simulation.worker', { type: 'module' }),
    this.simulationRequest.asObservable())
    .subscribe( (data) => this.dataset.next(data));

    this.reset();
  }

  reset() {
    this.parameters = Object.assign({},
      ...Object.entries(PARAMETER_PROPERTIES).map(([k, v]) => ({[k]: v.default})));
    this.simulationRequest.next({ parameters: this.parameters, update: -1 });
  }

  update( key: string, value: number ) {
    if (this.parameters[key] !== value) {
      this.parameters[key] = value;
      this.simulationRequest.next({ parameters: this.parameters, update: -1} );
    }
  }

  series( key: string, minValue: number, maxValue: number, steps: number) {
    const delta = (maxValue - minValue) / steps;
    for (let i = 0; i <= steps; i++) {
       this.simulationRequest.next({
        parameters: this.parameters,
        update: i,
        parameterKey: key,
        parameterValue: minValue + i * delta
       });
    }
  }

  result(): Observable<SimulationResult> {
    return this.dataset.asObservable();
  }

  ngOnDestroy(): void {
    this.simulationRequest.complete();
  }

  save() {
    const data = this.dataset.getValue();

    const { WIinfections, WIsick, WIoutpatients, WIhospitalisations, WIicu, WIdeaths,
      update, ...filteredData } = data;

    const headers = [ 'Day', ...Object.keys(filteredData).map(d => data[d].label)].join(', ');

    const rows = [];
    for (let day = 0; day <= this.parameters.simulation_days; day++) {
      const row = [ day, ...Object.keys(filteredData).map(d => data[d].values[day].y)].join(', ');
      rows.push( row );
    }

    const results = [ headers, ...rows ].join('\n');

    const blob = new Blob([results], { type: 'text/comma-separated-values'} );

    saveAs( blob, 'CovidSIM-results.csv');
  }

}
