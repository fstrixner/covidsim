import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ImpressumComponent } from '../dialogs/impressum/impressum.component';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  copyright = '2020 by ExploSYS GmbH';

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openImpressumDialog() {
    this.dialog.open(ImpressumComponent, { width: '900px' });
  }

}
