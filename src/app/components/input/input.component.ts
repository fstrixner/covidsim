import { Component, Input, OnInit } from '@angular/core';
import { ModelService } from '../../services/model.service';
import { ParameterProperties } from 'src/app/model/parameter-properties';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  @Input() parameter: ParameterProperties;

  constructor(private model: ModelService) { }

  ngOnInit() {
  }

  doInput(value: number) {
    // console.log('input', value);
    this.parameter.default = value;
  }

  change(value) {
    // console.log('change', value);
    this.parameter.default = value;
    this.model.update( this.parameter.key, +value );
  }

  keydown(event) {
    // console.log('keydown', event);
    if (event.keyCode === 13) {
      // console.log('return', event);
      this.change(event.srcElement.valueAsNumber);
      this.click(event);
    }
  }

  click(event) {
    // console.log('click', event);
    event.preventDefault();
    event.stopPropagation();
  }

  blur(value) {
    // console.log('blur', value);
    this.change(value);
  }

}
