import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ModelService } from '../../../services/model.service';
import { ParameterProperties } from 'src/app/model/parameter-properties';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent {


  @Input() parameter: ParameterProperties;

  constructor(private model: ModelService) { }

  input(value: number) {
    this.parameter.default = value;

  }

  change(value: number) {
    this.parameter.default = value;
    this.model.update( this.parameter.key, value );
  }
}
