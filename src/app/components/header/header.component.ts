import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModelService } from 'src/app/services/model.service';
import { AboutComponent } from '../dialogs/about/about.component';
import { HelpComponent } from '../dialogs/help/help.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  title = 'CovidSIM';
  subtitle = 'Pandemic Preparedness Planning for COVID-19';
  version = 'Version 1.1';

  versions = [
    { label: 'version 1.1', link: 'http://covidsim.eu/version-1.1' },
    { label: 'version 1.0', link: 'http://covidsim.eu/version-1.0' }
  ];

  currentVersion = this.versions[0];

  constructor(
    private dialog: MatDialog,
    private model: ModelService
  ) { }

  ngOnInit() {
  }

  openAboutDialog() {
    this.dialog.open(AboutComponent, { width: '700px' });
  }

  openHelpDialog() {
    this.dialog.open(HelpComponent, { width: '900px' });
  }

  redirect(version) {
    console.log('redirect to', version);
    window.location.href = version.link;
  }

  export() {
    this.model.save();
  }
}
