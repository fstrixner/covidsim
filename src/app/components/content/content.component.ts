import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModelService } from 'src/app/services/model.service';
import { NvD3Component } from 'ng2-nvd3';
import { PARAMETER_PROPERTIES } from 'src/app/model/parameter-properties';
import { SimulationResult } from 'src/app/model/simulation-result';

declare let d3: any;

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: [
    '../../../../node_modules/nvd3/build/nv.d3.css',
    './content.component.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class ContentComponent implements OnInit {

  blue = { borderColor: 'blue' };
  red = { borderColor: 'red' };
  green = { borderColor: 'green' };
  black = { borderColor: 'black' };
  pink = { borderColor: 'pink' };

  days;
  options;
  legend;
  colors = [ this.blue, this.red, this.green, this.black, this.pink ];

  sparkLine = {
    chart: {
      type: 'multiChart',
      height: 100,
      margin : { top: 30, right: 30, bottom: 20, left: 85 },
      x(d) { return d.x; },
      y(d) { return d.y; },
      showLegend: false,
      useInteractiveGuideline: true,
      interactiveLayer: {
        tooltip: {
          valueFormatter: (d) => d3.format(',.1f')(d)
        }
      },
      xAxis: { ticks: null },
      yAxis1: { axisLabel: 'R\u2080', ticks: null },
      yAxis2: {
        axisLabel: 'R\u2080',
        axisLabelDistance: 0,
        rotateYLabel: false,
        ticks: null }
    }
  };


  data: SimulationResult;
  data0;
  data1;
  data2;
  data3;
  data4;
  data5;
  data6 = [];

  incidence = 'cumulative';

  parameter;
  parameterKeys;
  key = 'basic_reproduction_number';
  minValue = 2;
  maxValue = 4;
  steps = 4;

  chart;
  testdata;

  subscription: Subscription;

  ngOnInit(): void {
  }

  constructor( public model: ModelService ) {
    this.parameter = PARAMETER_PROPERTIES;
    this.parameterKeys = ['basic_reproduction_number'];

    this.subscription = model.result()
    .subscribe( data => {

      if (data) {
        if (data.update < 0) {
          this.update(data);
        } else {
          this.series(data);
        }
      }

    });
  }

  update(data: SimulationResult) {
    if (this.data) {
      console.log('data', data, Object.keys(data));
      Object.keys(data).forEach( key => {
        if (key !== 'update') {
          this.data[key].values = [...data[key].values];
        }
      });
    } else {
      this.data = data;
    }
    if (this.data) {
      const d = this.data;
      this.data0 = [ d.r0 ];
      this.data1 = [ d.susceptible, d.infected, d.recovered, d.dead, d.detection, d.contactReduction ];
      this.data2 = [ d.latent, d.prodromal, d.asymptomatic, d.sick,
                     d.hospitalized, d.icu, d.isolated, d.secluded ];
      this.data3 = [ d.CIinfections, d.CIsick, d.CIoutpatients,
                     d.CIhospitalisations, d.CIicu, d.CIdeaths ];
      this.data4 = [ d.DIinfections, d.DIsick, d.DIoutpatients,
                     d.DIhospitalisations, d.DIicu, d.DIdeaths ];
      this.data5 = [ d.WIinfections, d.WIsick, d.WIoutpatients,
                     d.WIhospitalisations, d.WIicu, d.WIdeaths ];
    }
  }

  series(data) {
    console.log('series---', data[1]);
    const index = data[0].update;
    const series = data[0].series;
    const value = data[0].value;
    console.log('series', index, series, value, data);

    if (this.data6[index]) {
      this.data6[index].values = data[1].values;
    } else {
      this.data6[index] = data[1];
    }

    this.data6 = [...this.data6];

    this.data6[index].area = false;

    this.data6[index].key = value;

    console.log('data6', this.data6);

  }
}
