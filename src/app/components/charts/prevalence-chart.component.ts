import { Component, OnInit, Input } from '@angular/core';
import { Dataset } from 'src/app/model/simulation-result';

@Component({
  selector: 'app-prevalence-chart',
  template: '<nvd3 [options]="lineChart" [data]="data"></nvd3>',
  styleUrls: ['../../../../node_modules/nvd3/build/nv.d3.css']
})
export class PrevalenceChartComponent implements OnInit {

  @Input() data: Dataset[];

  lineChart = {
    chart: {
      type: 'lineChart',
      height: 500,
      width: 850,
      margin : { top: 40, right: 30, bottom: 50, left: 85 },
      x(d) { return d.x; },
      y(d) { return d.y; },
      legend: {
        align: false,
      },
      showValues: true,
      focusEnable: true,
      useInteractiveGuideline: true,
      legendRightAxisHint: '',
      duration: 0,
      xAxis: { axisLabel: 'Day' },
      yAxis: {
        axisLabel: 'Individuals',
        axisLabelDistance: 25,
        tickFormat: (d) => d3.format(',.0f')(d)
      }
    }
  };

  constructor() { }

  ngOnInit() {
  }

}
