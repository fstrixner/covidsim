import { Component, OnInit, Input } from '@angular/core';
import { Dataset } from 'src/app/model/simulation-result';

@Component({
  selector: 'app-sensitivity-chart',
  template: '<nvd3 [options]="multiChart" [data]="data"></nvd3>',
  styleUrls: ['../../../../node_modules/nvd3/build/nv.d3.css']
})
export class SensitivityChartComponent implements OnInit {

  @Input() data: Dataset[];

  multiChart = {
    chart: {
      type: 'multiChart',
      height: 500,
      width: 850,
      margin : { top: 40, right: 30, bottom: 50, left: 85 },
      x(d) { return d.x; },
      y(d) { return d.y; },
      legend: {
        align: false,
      },
      interactiveLayer: {
        tooltip: {
          valueFormatter: (d, i) =>
          (i === 4) ? d3.format(',.1f')(d) : d3.format(',.0f')(d)
        }
      },
      showValues: true,
      focusEnable: true,
      useInteractiveGuideline: true,
      legendRightAxisHint: '',
      duration: 0,
      xAxis: { axisLabel: 'Day' },
      yAxis1: {
        axisLabel: 'Individuals',
        axisLabelDistance: 25,
        tickFormat: (d) => d3.format(',.0f')(d)
      },
      yAxis2: {
        axisLabel: 'Detection Probability',
        axisLabelDistance: 0,
      }
    }
  };

  constructor() { }

  ngOnInit() {
  }

}
