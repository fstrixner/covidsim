import { ParameterSettings } from './parameter-settings';

export interface SimulationRequest {
  parameters: ParameterSettings;
  update: number;
  parameterKey?: string;
  parameterValue?: number;
}
