export interface ParameterProperties {
  key: string;
  label: string;
  unit: string;
  min: number;
  max: number;
  step: number;
  default: number;
  default_min?: number;
  default_max?: number;
  description?: string;
}

export const PARAMETER_PROPERTIES: { [id: string]: ParameterProperties } = {
  population_size: {
    key: 'population_size',
    label: 'Population size',
    unit: 'million',
    min: 1,
    max: 1000,
    step: 1,
    default: 100,
    description: `
    The total size of the population. As CovidSIM is a rather simple tool,
    the whole population is assumed to be homogenously mixing.
    `
  },
  initial_cases: {
    key: 'initial_cases',
    label: 'Initial infections',
    unit: undefined,
    min: 1,
    max: 1000,
    step: 1,
    default_min: 1,
    default_max: 100,
    default: 1,
    description: `
    Determines the number of individuals who are infected at the beginning
    of the simulation. The remaining population is assumed to be non-immune.
    <br><red>We recommend that you do not change this value.</red><br>
    It is not a good idea to set it to the number of cases who have already
    been identified and isolated, because they should not be able to spread
    the infection in the population.
    It may be more relevant to assume that at some unknown time point one
    person (or a few persons) have brought in the infection into a population,
    but have remained undetected, and to see how the infection is spreading
    in such a scenario.
    The detection probability (see below) can then be used to see how far
    this infection has spread before it actually is detected by a random
    SARS-CoV-2 test.
    `
  },
  external_force_of_infection: {
    key: 'external_force_of_infection',
    label: 'Infections from outside of the population',
    unit: 'per day',
    min: 0,
    max: 100,
    step: 1,
    default_min: 0,
    default_max: 100,
    default: 1,
    description: ``
  },
  simulation_days: {
    key: 'simulation_days',
    label: 'Simulation duration',
    unit: 'days',
    min: 0,
    max: 1000,
    step: 1,
    default: 365,
    description: ``
  },
  latency_duration: {
    key: 'latency_duration',
    label: 'Latency period',
    unit: 'days',
    min: 1,
    max: 10,
    step: 0.1,
    default_min: 3,
    default_max: 5,
    default: 4,
    description: `
    Determines the average duration of the latency period.
    This is the initial period of an infection during which the infected
    individual cannot spread the infection to others.
    `
  },
  prodromal_duration: {
    key: 'prodromal_duration',
    label: 'Prodromal period',
    unit: 'days',
    min: 0,
    max: 5,
    step: 0.1,
    default_min: 0,
    default_max: 2,
    default: 1,
    description: `
    Determines the average duration of the period which immediately
    follows the latency period. During this period, some patients may
    show mild and untypical symptoms. CovidSIM also allows to assume
    that infected individuals can spread the virus to others during
    the prodromal period (see Contagiousness parameters below).
    `
  },
  infective_duration: {
    key: 'infective_duration',
    label: 'Fully infective period',
    unit: 'days',
    min: 5,
    max: 20,
    step: 0.1,
    default_min: 6,
    default_max: 14,
    default: 10,
    description: `
    Determines the period which follows the prodromal period.
    This is typically the period during which clear symptoms occur and
    during which cases infect others, but we use the same duration for
    the infectiousness for individuals who do not develop symptoms.
    At the end of this period, the vast majority of cases acquires a
    full immunity, but a small fraction of them will die.
    `
  },
  declining_duration: {
    key: 'declining_duration',
    label: 'Declining period',
    unit: 'days',
    min: 0,
    max: 20,
    step: 0.1,
    default_min: 6,
    default_max: 14,
    default: 0,
    description: `
    Determines the period which follows the prodromal period.
    This is typically the period during which clear symptoms occur and
    during which cases infect others, but we use the same duration for
    the infectiousness for individuals who do not develop symptoms.
    At the end of this period, the vast majority of cases acquires a
    full immunity, but a small fraction of them will die.
    `
  },
  hospitalisation_duration: {
    key: 'hospitalisation_duration',
    label: 'Hospitalisation',
    unit: 'days',
    min: 0,
    max: 50,
    step: 0.5,
    default: 14,
    description: `
    Determines the length of stay in hospital.
    `
  },
  icu_duration: {
    key: 'icu_duration',
    label: 'ICU admission',
    unit: 'days',
    min: 0,
    max: 50,
    step: 0.5,
    default: 14,
    description: `
    Determines the length of stay in intensive care.
    `
  },
  latency_stages: {
    key: 'latency_stages',
    label: 'Number of latency stages',
    unit: undefined,
    min: 1,
    max: 32,
    step: 1,
    default_min: 1,
    default_max: 32,
    default: 16,
    description: `
    Determines the variability of the duration of the latency period
    (assuming an Erlang distribution). Using 16 stages, the standard
    deviation of the symptomatic period is 25% of its mean duration.
    `
  },
  prodromal_stages: {
    key: 'prodromal_stages',
    label: 'Number of prodromal stages',
    unit: undefined,
    min: 1,
    max: 32,
    step: 1,
    default_min: 1,
    default_max: 32,
    default: 16,
    description: `
    Determines the variability of the duration of the prodromal period
    (assuming an Erlang distribution). Using 16 stages, the standard
    deviation of the symptomatic period is 25% of its mean duration.
    `
  },
  infective_stages: {
    key: 'infective_stages',
    label: 'Number of infective stages',
    unit: undefined,
    min: 1,
    max: 32,
    step: 1,
    default_min: 1,
    default_max: 32,
    default: 16,
    description: `
    Determines the variability of the duration of the infective period
    (assuming an Erlang distribution). Using 16 stages, the standard
    deviation of the symptomatic period is 25% of its mean duration.
    `
  },
  declining_stages: {
    key: 'declining_stages',
    label: 'Number of declining stages',
    unit: undefined,
    min: 1,
    max: 32,
    step: 1,
    default_min: 1,
    default_max: 32,
    default: 16,
    description: `
    Determines the variability of the duration of the declining period
    (assuming an Erlang distribution). Using 16 stages, the standard
    deviation of the symptomatic period is 25% of its mean duration.
    `
  },
  erlang_stages: {
    key: 'erlang_stages',
    label: 'Number of Erlang stages',
    unit: undefined,
    min: 1,
    max: 32,
    step: 1,
    default_min: 1,
    default_max: 32,
    default: 16,
    description: `
    Determines the variability of the duration of the course of disease periods
    (assuming an Erlang distribution). Using 16 stages, the standard
    deviation of the symptomatic period is 25% of its mean duration.
    `
  },
  symptomatic_course_of_disease: {
    key: 'symptomatic_course_of_disease',
    label: 'Infections which will lead to sickness',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default_min: 20,
    default_max: 100,
    default: 82,
    description: `
    Determines the percentage of infected individuals who eventually
    become sick. The remaining ones will not have any symptoms, but can
    infect others in the same way and for the same duration as sick ones.
    If more information will become available, we can extend the model to
    give them a different contagiousness, but at this early stage of
    knowledge, we assume that people who are not sick (although they may
    spread fewer virus particles) infect the same number of people as sick
    patients (who spread more virus particles), because they are able to
    leave home and meet many others.
    `
  },
  medical_consultations_by_symptomatic_cases: {
    key: 'medical_consultations_by_symptomatic_cases',
    label: 'Sick patients seek medical help',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default_min: 20,
    default_max: 60,
    default: 40,
    description: `
    Determines what percentage of sick cases go to the doctor to seek medical help.
    `
  },
  hospitalisation_of_symptomatic_cases: {
    key: 'hospitalisation_of_symptomatic_cases',
    label: 'Sick patients are hospitalized',
    unit: '%',
    min: 0,
    max: 100,
    step: 0.5,
    default_min: 0.1,
    default_max: 50,
    default: 0.6,
    description: `
    Determines the percentage of sick cases who are hospitalized.<br>
    This parameter is based on the German report on cases with
    influenza-like illness from 2017/18: with 40% of cases seeking
    medical help, 9 million medical consultations refer to about
    22.5 million symptomatic cases; the estimated number of 45,000
    hospitalizations are 0.2% of the symptomatic cases.
    `
  },
  icu_patients_among_hospitalized_cases: {
    key: 'icu_patients_among_hospitalized_cases',
    label: 'Hospitalized cases need intensive care (ICU)',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default_min: 15,
    default_max: 35,
    default: 25,
    description: `
    Determines the percentage of hospitalized cases who will be admitted
    to an intensive care unit (ICU).
    `
  },
  case_fatality_of_symptomatic_cases: {
    key: 'case_fatality_of_symptomatic_cases',
    label: 'Sick patients die from the disease',
    unit: '%',
    min: 0,
    max: 100,
    step: 0.1,
    default_min: 0.1,
    default_max: 10,
    default: 0.3,
    description: `
    Determines the percentage of sick cases who die from the disease.
    `
  },
  basic_reproduction_number: {
    key: 'basic_reproduction_number',
    label: 'Annual average of the basic reproduction number R\u2080',
    unit: undefined,
    min: 1,
    max: 10,
    step: 0.1,
    default_min: 2.7,
    default_max: 4.4,
    default: 4,
    description: `
    Determines the average number of infections which are caused
    by a single infected individual in a population where nobody
    is immune and where nobody takes any preventive measures (no
      contact reduction, no isolation, no treatment etc.).
    It is important to note that this only refers to people who are
    infected by the "index case", but it does not include infections
    which are caused by the infected people themselves.
    Other parameters like the duration of the infective period (see above)
    are already factored in the basic reproduction number.<br>
    As a consequence of this, if the value 4 is chosen, one infected
    individual infects on average 4 others; if the duration of the
    infectious period is doubled, the infected individual still infects
    4 others (not 8 as one may assume), but then it takes about twice as
    long until these 4 are infected;
    thus doubling the infective period does not double the number of
    secondary infections, but only slows down the spread of the infection.
    If the user wants to obtain a higher duration of infectiveness and
    also a higher number of secondary cases, both parameters have to be changed.
    `
  },
  maximal_seasonal_transmission_factor: {
    key: 'maximal_seasonal_transmission_factor',
    label: 'Amplitude of the seasonal fluctuation of R\u2080',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default_min: 0,
    default_max: 100,
    default: 0,
    description: ``
  },
  maximal_seasonal_transmission_day: {
    key: 'maximal_seasonal_transmission_day',
    label: 'Day when the seaonal R₀ reaches its maximum',
    unit: undefined,
    min: 0,
    max: 365,
    step: 1,
    default: 0,
    description: ``
  },
  prodromal_contagiousness: {
    key: 'prodromal_contagiousness',
    label: 'Relative contagiousness in the prodromal period',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default_min: 0,
    default_max: 100,
    default: 50,
    description: `
    Determines how contagious cases are during their prodromal period
    (see section Durations).<br>
    The contagiousness during the symptomatic period (which follows the
    prodromal period) is basically calculated from the basic reproduction
    number and from the average duration of the symptomatic period. This
    contagiousness is then used as the 100% reference to calculate the
    contagiousness during the prodromal period. If a value of 50% is used
    here, this means that individuals in their early infective period
    (which we call "prodromal period") are only half as contagious as in
    their late infective period (which we call "symptomatic period").
    `
  },
  declining_contagiousness: {
    key: 'declining_contagiousness',
    label: 'Relative contagiousness in the declining period',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default_min: 0,
    default_max: 100,
    default: 100,
    description: `
    Determines how contagious cases are during their declining period
    (see section Durations).<br>
    The contagiousness during the symptomatic period (which follows the
    prodromal period) is basically calculated from the basic reproduction
    number and from the average duration of the symptomatic period. This
    contagiousness is then used as the 100% reference to calculate the
    contagiousness during the prodromal period. If a value of 50% is used
    here, this means that individuals in their early infective period
    (which we call "prodromal period") are only half as contagious as in
    their late infective period (which we call "symptomatic period").
    `
  },

  symptomatic_cases_to_be_isolated: {
    key: 'symptomatic_cases_to_be_isolated',
    label: 'Probability that a sick patient is isolated ',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default_min: 0,
    default_max: 100,
    default: 0,
    description: ``
  },
  case_isolation_capacity: {
    key: 'case_isolation_capacity',
    label: 'Maximum capacity of isolation wards',
    unit: 'per 10,000',
    min: 0,
    max: 1000,
    step: 1,
    default_min: 0,
    default_max: 10,
    default: 0,
    description: `
    Determines how many cases can be isolated in a population.
    Only sick cases who seek medical help can be isolated in our simulation.
    For the time during which this intervention is switched on, severe cases
    are immediately isolated when they show symptoms (i.e. at the end of
      their prodromal period and the beginning of their symptomatic period).
    They will be isolated until the end of their symptomatic period.
    Isolated cases are completely prevented from spreading the infection.
    <br>
    Please note that these are rather optimistic assumptions;
    in reality, isolation may frequently occur at a later stage of the
    infection, and cases will occupy the isolation units much longer
    than their infective period.
    <br>
    If the number of sick cases exceeds the the isolation capacity which is
    determined by this parameter, the surplus number of them cannot be isolated,
    but will be sent into home isolation.
    `
  },
  contact_reduction_home_isolation: {
    key: 'contact_reduction_home_isolation',
    label: 'Contact reduction for cases in home isolation',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default_min: 0,
    default_max: 100,
    default: 0,
    description: ``
  },
  case_isolation_begin: {
    key: 'case_isolation_begin',
    label: 'Begin of case isolation measures',
    unit: 'day',
    min: 0,
    max: 365,
    step: 1,
    default_min: 60,
    default_max: 120,
    default: 0,
    description: `Determines when the isolation measures start.`
  },
  case_isolation_range: {
    key: 'case_isolation_range',
    label: 'Duration of case isolation measures',
    unit: 'days',
    min: 0,
    max: 365,
    step: 1,
    default_min: 30,
    default_max: 90,
    default: 0,
    description: `
    Determines how long the isolation measures are performed.
    <br>
    This does not determine how long a single case is isolated.
    As stated above, cases are always isolated until the end of
    their symptomatic period.
    `
  },
  contact_reduction_factor: {
    key: 'contact_reduction_factor',
    label: 'General contact reduction',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default_min: 0,
    default_max: 50,
    default: 0,
    description: `
    Determines what percentage of contacts are prevented in the general
    population. This summarizes a wide variety of different interventions:
    people may generally have fewer contacts; they may enforce personal
    hygiene measures (hand washing etc.) or wear face masks to effectively
    reduce their contacts; schools may be closed and mass gatherings may
    be stopped.
    `
  },
  contact_reduction_begin: {
    key: 'contact_reduction_begin',
    label: 'Contact reduction begin',
    unit: 'day',
    min: 0,
    max: 365,
    step: 1,
    default_min: 60,
    default_max: 120,
    default: 0,
    description: `Determines when the contact reduction starts`
  },
  contact_reduction_range: {
    key: 'contact_reduction_range',
    label: 'Contact reduction duration',
    unit: 'days',
    min: 0,
    max: 365,
    step: 1,
    default_min: 30,
    default_max: 90,
    default: 0,
    description: `
    Determines how long the contact reduction lasts. After the end of
    this period, contacts are set back to the original 100% value.
    `
  },
  sick_threshold: {
    key: 'sick_threshold',
    label: 'Sick Treshold',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default: 0,
    description: `
    Determines the fraction of sick cases in the population that triggers
    contact reduction`
  },
  sick_triggered_contact_reduction: {
    key: 'sick_triggered_contact_reduction',
    label: 'Sick triggered contact reduction',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default: 0,
    description: `
    Determines the percentage of contacts prevented when triggered by sick cases`
  },
  hospitalisation_threshold: {
    key: 'hospitalisation_threshold',
    label: 'Hospitalisation Threshold',
    unit: 'per 1.000',
    min: 0,
    max: 1000,
    step: 1,
    default: 0,
    description: `
    Determines the number of hospitalised cases per 1000 that triggers contact reduction
    `
  },
  hospitalisation_triggered_contact_reduction: {
    key: 'hospitalisation_triggered_contact_reduction',
    label: 'Hospitalisation triggered contact reduction',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default: 0,
    description: `
    Determines the percentage of contacts prevented when triggered by hospitalised cases
    `
  },
  icu_threshold: {
    key: 'icu_threshold',
    label: 'ICU Threshold',
    unit: 'per 100.000',
    min: 0,
    max: 1000,
    step: 1,
    default: 0,
    description: `
    Determines the number of ICU admissions per 100.000 population that triggers
    contact reduction
    `
  },
  icu_triggered_contact_reduction: {
    key: 'icu_triggered_contact_reduction',
    label: 'ICU triggered contact reduction',
    unit: '%',
    min: 0,
    max: 100,
    step: 1,
    default: 0,
    description: `
    Determines the percentage of contacts prevented when triggered by sick cases
    `
  },
  detection_probability_medical_consultation: {
    key: 'detection_probability_medical_consultaion',
    label: 'in ILI patients who seek medical help',
    unit: '%',
    min: 0,
    max: 100,
    step: 0.1,
    default_min: 0,
    default_max: 100,
    default: 0.1,
    description: `
    Determines the percentage of randomly picked ILI patients
    who seek medical help for whom a SARS-CoV-2 test is performed.
    `
  },
  detection_probability_hospitalisation: {
    key: 'detection_probability_hospitalisation',
    label: 'in hospitalized ILI patients',
    unit: '%',
    min: 0,
    max: 100,
    step: 0.1,
    default_min: 0,
    default_max: 100,
    default: 0.1,
    description: `
    Determines the percentage of randomly picked ILI patients
    who are admitted to a hospital for whom a SARS-CoV-2 test is performed.
    `
  },
  detection_probability_deceased_case: {
    key: 'detection_probability_deceased_case',
    label: 'in patients who died from ILI',
    unit: '%',
    min: 0,
    max: 100,
    step: 0.1,
    default_min: 0,
    default_max: 100,
    default: 0.1,
    description: `
    determines the percentage of randomly picked ILI patients
    who have died from the disease and for whom a SARS-CoV-2
    test is performed.
    `
  }
};

