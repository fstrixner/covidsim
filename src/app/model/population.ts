import { ParameterSettings } from './parameter-settings';

const sum = (a: number, b: number) => a + b;

export class Population {

  offset;

  stages;

  params: ParameterSettings;

  numberOfEquations: number;

  constructor( params: ParameterSettings ) {

    this.params = params;

    const S = 0;
    const E = S + 1;
    const P = E + params.latency_stages;
    const I = P + params.prodromal_stages;
    const C = I + params.infective_stages;
    const R = C + params.declining_stages;

    const CII = R + 1;
    const CIC = CII + 1;

    const Isolated = CIC + 1;
    const Secluded = Isolated + 1;

    this.numberOfEquations = Secluded + 1;
    this.offset = { S, E, P, I, C, R, CII, CIC, Isolated, Secluded };

    const latent = params.latency_stages;
    const prodromal = params.prodromal_stages;
    const infective = params.infective_stages;
    const convalescent = params.declining_stages;

    this.stages = { latent, prodromal, infective, convalescent };
  }

  equations() {
    return this.numberOfEquations;
  }

  susceptible(y: number[]): number {
    return y[this.offset.S];
  }

  latent(y: number[]): number {
    return y.slice(this.offset.E, this.offset.E + this.params.latency_stages).reduce(sum, 0);
  }

  prodromal(y: number[]): number {
    return y.slice(this.offset.P, this.offset.P + this.params.prodromal_stages).reduce(sum, 0);
  }

  infective(y: number[]): number {
    return y.slice(this.offset.I, this.offset.I + this.params.infective_stages).reduce(sum, 0);
  }

  convalescent(y: number[]): number {
    return y.slice(this.offset.C, this.offset.C + this.params.declining_stages).reduce(sum, 0);
  }

  removed(y: number[]): number {
    return y[this.offset.R];
  }

  cumulativeInfections(y: number[]): number {
    return y[this.offset.CII];
  }

  cumulativeCases(y: number[]): number {
    return y[this.offset.CIC];
  }

  isolated(y: number[]): number {
    return y[this.offset.Isolated];
  }

  homeIsolated(y: number[]): number {
    return y[this.offset.Secluded];
  }

}
