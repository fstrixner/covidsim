import { DoWorkUnit, ObservableWorker } from 'observable-webworker';
import {SEPIRstagesSimulation} from './simulation';
import {SimulationRequest} from './simulation-request';
import {SimulationResult} from './simulation-result';
import {Observable, of} from 'rxjs';

@ObservableWorker()
export class SEPIRstagesWorker extends SEPIRstagesSimulation  implements DoWorkUnit<SimulationRequest, SimulationResult> {

  public workUnit(request: SimulationRequest): Observable<SimulationResult> {
    console.log('Simulation Request', request);
    return of( this.simulate(request) );
  }
}
