import { Dgl } from './runge-kutta-solver';
import { ParameterSettings } from './parameter-settings';
import { Population } from './population';

const SINUSFACTOR = 2 * Math.PI / 365;

interface Flow {
  source: number;
  target: number;
  rate: number;
  counter?: number;
}

export class SEPIRstagesDgl implements Dgl {

  flows: Flow[];

  settings = {
    contactReduction: 1,
    isolationCapacity: 0
  };

  popsize: number;
  prodromalContagiousness: number;
  decliningContagiousness: number;
  homeIsolationContagiousness: number;
  symptomaticFraction: number;
  isolatedFraction: number;

  externalFoI: number;

  maxSeasonalTransmissionFactor: number;
  maxSeasonalTransmissionDay: number;

  rateSE: number;

  population: Population;

  sum = (a: number, b: number) => a + b;

  init( params: ParameterSettings ): number[] {

    this.population = new Population(params);

    this.popsize = params.population_size * 1000000;
    this.externalFoI = params.external_force_of_infection / this.popsize;

    this.maxSeasonalTransmissionFactor = params.maximal_seasonal_transmission_factor / 100;
    this.maxSeasonalTransmissionDay = params.maximal_seasonal_transmission_day;

    this.prodromalContagiousness = params.prodromal_contagiousness / 100;
    this.decliningContagiousness = params.declining_contagiousness / 100;
    this.homeIsolationContagiousness = 1 - params.contact_reduction_home_isolation / 100;

    this.symptomaticFraction = params.symptomatic_course_of_disease / 100;
    this.isolatedFraction = params.symptomatic_cases_to_be_isolated / 100;

    this.rateSE = params.basic_reproduction_number /
                ( params.prodromal_duration * this.prodromalContagiousness
                + params.infective_duration
                + params.declining_duration * this.decliningContagiousness);

    const rateEP = params.latency_stages / params.latency_duration;
    const ratePI = params.prodromal_stages / params.prodromal_duration;
    const rateIC = params.infective_stages / params.infective_duration;
    const rateCR = params.declining_stages / params.declining_duration;

    // console.log('rates', this.rateIC, this.rateCR);

    const doProdromalPeriod = params.prodromal_duration > 0;
    const doDecliningPeriod = params.declining_duration > 0;

    const E = (stage: number) => this.population.offset.E + stage;
    const P = (stage: number) => this.population.offset.P + stage;
    const I = (stage: number) => this.population.offset.I + stage;
    const C = (stage: number) => this.population.offset.C + stage;
    const R = () => this.population.offset.R;

    this.flows = [];

    for (let i = 1; i < params.latency_stages; i++) {
      this.flows.push({ source: E(i - 1), target: E(i), rate: rateEP });
    }

    if (doProdromalPeriod) {
      this.flows.push({ source: E( params.latency_stages - 1), target: P(0), rate: rateEP });

      for (let i = 1; i < params.prodromal_stages; i++) {
        this.flows.push({source: P(i - 1), target: P(i), rate: ratePI });
      }

      this.flows.push({ source: P(params.prodromal_stages - 1), target: I(0), rate: ratePI,
        counter: this.population.offset.CIC });

    } else {

      this.flows.push({ source: E(params.latency_stages - 1), target: I(0), rate: rateEP });

    }

    for (let i = 1; i < params.infective_stages; i++) {
      this.flows.push({ source: I(i - 1), target: I(i), rate: rateIC });
    }

    if (doDecliningPeriod) {

      this.flows.push({ source: I(params.infective_stages - 1), target: C(0), rate: rateIC });

      for (let i = 1; i < params.declining_stages; i++) {
        this.flows.push({ source: C(i - 1), target: C(i), rate: rateCR });
      }

      this.flows.push({ source: C(params.declining_stages - 1), target: R(), rate: rateCR });

    } else {

      this.flows.push({ source: I(params.infective_stages - 1), target: R(), rate: rateIC });

    }

    const susceptibles = this.popsize - params.initial_cases;
    const out = new Array<number>(this.population.equations()).fill(0);
    out[this.population.offset.S] = susceptibles;
    out[this.population.offset.E] = params.initial_cases;

    // console.log('Initial Population', out);

    return out;
  }

  set(key: string, value: number | boolean) {
    this.settings[key] = value;
  }

  eval(x: number, y: number[], dy: number[]) {

    dy.fill(0);



    const offset = this.population.offset;

    const prodromalCases    = this.population.prodromal(y);
    const illnessCases    = this.population.infective(y);
    const declineCases = this.population.convalescent(y);
    const infectiveCases = illnessCases + declineCases;
    const detectedCases = infectiveCases * this.symptomaticFraction * this.isolatedFraction;

    const isolatedCases = (this.settings.isolationCapacity < 0) ? 0 :
    Math.min(detectedCases, this.settings.isolationCapacity);
    const isolatedIllnessCases = (infectiveCases === 0) ? 0 : isolatedCases * illnessCases / infectiveCases;
    const isolatedDeclineCases = isolatedCases - isolatedIllnessCases;

    const homeIsolatedCases = (this.settings.isolationCapacity < 0) ? 0 : detectedCases - isolatedCases;
    const homeIsolatedIllnessCases = (detectedCases === 0) ? 0 : homeIsolatedCases * illnessCases / infectiveCases;
    const homeIsolatedDeclineCases = homeIsolatedCases - homeIsolatedIllnessCases;



    const FoI = ( prodromalCases * this.prodromalContagiousness
      + homeIsolatedIllnessCases * this.homeIsolationContagiousness
      + infectiveCases - isolatedIllnessCases - homeIsolatedIllnessCases
      + homeIsolatedDeclineCases * this.homeIsolationContagiousness * this.decliningContagiousness
      + (declineCases - isolatedDeclineCases - homeIsolatedDeclineCases) * this.decliningContagiousness
    ) / this.popsize;

    const currentSeasonalFactor = 1 + this.maxSeasonalTransmissionFactor
         * Math.cos(SINUSFACTOR * ( x - this.maxSeasonalTransmissionDay ));

    const rate = this.rateSE * FoI * currentSeasonalFactor * this.settings.contactReduction + this.externalFoI;

    if (x < 0) {
      console.log('prodromalCases', prodromalCases);
      console.log('illnessCases', illnessCases);
      console.log('declineCases', declineCases);
      console.log('infectiveCases', infectiveCases);
      console.log('detectedCases', detectedCases);
      console.log('isolatedCases', isolatedCases);
      console.log('isolatedIllnessCases', isolatedIllnessCases);
      console.log('isolatedDeclineCases', isolatedDeclineCases);
      console.log('homeIsolatedCases', homeIsolatedCases);
      console.log('homeIsolatedIllnessCases', homeIsolatedIllnessCases);
      console.log('homeIsolatedDeclineCases', homeIsolatedDeclineCases);
      console.log('symptomaticFraction', this.symptomaticFraction);
      console.log('isolatedFraction', this.isolatedFraction);
      console.log('isolationCapacity', this.settings.isolationCapacity);
      console.log('forceOfInfection', FoI);
      console.log('rateSE', this.rateSE);
      console.log('currentSeasonalFactor', currentSeasonalFactor);
      console.log('contacReduction', this.settings.contactReduction);
      console.log('externalFoI', this.externalFoI);
      console.log('rate', rate);
    }

    const doFlow =  (flow: Flow) => {
      const f =  y[flow.source] * flow.rate;
      dy[flow.source] -= f;
      dy[flow.target] += f;
      if (flow.counter) { dy[flow.counter] += f; }
    };

    doFlow({ source: offset.S, target: offset.E, rate, counter: offset.CII });

    this.flows.forEach( doFlow );

    if (x < 0) {
      console.log('Population on day', x, y, dy);
    }
  }
}
