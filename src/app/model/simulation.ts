import { Observable, of } from 'rxjs';

import { RungeKuttaSolver } from './runge-kutta-solver';
import { SEPIRstagesDgl } from './SEPIRstagesDgl';
import { SimulationRequest } from './simulation-request';
import { SimulationResult } from './simulation-result';
import { Population } from './population';

const rk4 = new RungeKuttaSolver();
const dgl = new SEPIRstagesDgl();

export class SEPIRstagesSimulation {

  public simulate( request: SimulationRequest, loggingEnabled: boolean = true ): SimulationResult {

    const params = request.parameters;

    const population = new Population( params ) ;

    const S = { values: [], key: 'Susceptible', label: 'Susceptible', color: 'blue', type: 'line', yAxis: 1,
      update: request.update, series: request.parameterKey, value: request.parameterValue };
    const I = { values: [], key: 'Infected', label: 'Infected', color: 'red', type: 'area', yAxis: 1 };
    const R = { values: [], key: 'Recovered', label: 'Recovered', color: 'green', type: 'line', yAxis: 1 };
    const D = { values: [], key: 'Dead', label: 'Dead', color: 'black', type: 'area', yAxis: 1 };
    const DP = { values: [], key: 'Detection Probability', label: 'Detection Probability', type: 'line', color: 'lightgrey', yAxis: 2};
    const CR = { values: [], key: 'Contact Reduction', label: 'Contact Reduction', color: 'lightgray', type: 'area', yAxis: 2};

    const R0 = { values: [], key: 'R\u2080', label: 'R0', color: 'maroon', type: 'line', yAxis: 2};

    const L = { values: [], key: 'Latent', label: 'Latent', color: 'olive', type: 'line', yAxis: 1 };
    const P = { values: [], key: 'Prodromal', label: 'Prodromal', color: '#FFC300', type: 'line', yAxis: 1};
    const A = { values: [], key: 'Symptomless Infectious', label: 'Symptomless Infectious', color: '#FF5733', type: 'line', yAxis: 1};
    const Y = { values: [], key: 'Sick', label: 'Sick', color: '#C70039', type: 'line', yAxis: 1};
    const H = { values: [], key: 'Hospitalized', label: 'Hospitalized', color: '#900C3F', type: 'line', yAxis: 1};
    const X = { values: [], key: 'ICU', label: 'ICU', color: '#581845', type: 'line', yAxis: 1};

    const IC = { values: [], key: 'Isolated', label: 'Isolated', color: 'green', type: 'line', yAxis: 1};
    const SC = { values: [], key: 'Home Isolated', label: 'Home Isolated', color: 'lawngreen', type: 'line', yAxis: 1};

    const CII = { values: [], key: 'Infections', label: 'CI Infections', color: 'olive', type: 'line', yAxis: 1, disabled: true };
    const CIS = { values: [], key: 'Sick', label: 'CI Sick', color: '#C70039', type: 'line', yAxis: 1};
    const CIM = { values: [], key: 'Med. Consult.', label: 'CI Med. Consult.', color: 'grey', type: 'line', yAxis: 1};
    const CIH = { values: [], key: 'Hospitalizations', label: 'CI Hospitalizations', color: '#900C3F', type: 'line', yAxis: 1};
    const CIX = { values: [], key: 'ICU Adm.', label: 'CI ICU Adm.', color: '#581845', type: 'line', yAxis: 1};
    const CID = { values: [], key: 'Deaths', label: 'CI Deaths', color: 'black', type: 'line', yAxis: 1};

    const DII = { values: [], key: 'Infections', label: 'DI Infections', color: 'olive', type: 'line', yAxis: 1, disabled: true};
    const DIS = { values: [], key: 'Sick', label: 'DI Sick', color: '#C70039', type: 'line', yAxis: 1};
    const DIM = { values: [], key: 'Med. Consult.', label: 'DI Med. Consult.', color: 'grey', type: 'line', yAxis: 1};
    const DIH = { values: [], key: 'Hospitalizations', label: 'DI Hospitalizations', color: '#900C3F', type: 'line', yAxis: 1};
    const DIX = { values: [], key: 'ICU Adm.', label: 'DI ICU Adm.', color: '#581845', type: 'line', yAxis: 1};
    const DID = { values: [], key: 'Deaths', label: 'DI Deaths', color: 'black', type: 'line', yAxis: 1};

    const WII = { values: [], key: 'Infections', label: 'WI Infections', color: 'olive', type: 'bar', yAxis: 1, disabled: true};
    const WIS = { values: [], key: 'Sick', label: 'WI Sick', color: '#C70039', type: 'bar', yAxis: 1};
    const WIM = { values: [], key: 'Med. Consult.', label: 'WI Med. Consult.', color: 'grey', type: 'bar', yAxis: 1, disabled: true};
    // tslint:disable-next-line:max-line-length
    const WIH = { values: [], key: 'Hospitalizations', label: 'WI Hospitalizations', color: '#900C3F', type: 'bar', yAxis: 1, disabled: true};
    const WIX = { values: [], key: 'ICU Adm.', label: 'WI ICU Adm.', color: '#581845', type: 'bar', yAxis: 1, disabled: true};
    const WID = { values: [], key: 'Deaths', label: 'WI Deaths', color: 'black', type: 'bar', yAxis: 1, disabled: true};

    const Popsize = { values: [], key: 'Popsize', label: 'Popupation Size', color: 'black' };

    const x0 = 0;
    const y0 = dgl.init( params );

    rk4.init( dgl, x0, y0);
    rk4.error(0.1);

    const SINUSFACTOR = 2 * Math.PI / 365;
    const symptomaticFraction = params.symptomatic_course_of_disease / 100;
    const medicalConsultations = params.medical_consultations_by_symptomatic_cases / 100;
    const hospitalizedFraction = params.hospitalisation_of_symptomatic_cases / 100;
    const icuFractionHospitalized = params.icu_patients_among_hospitalized_cases / 100;
    const caseFatality = params.case_fatality_of_symptomatic_cases / 100;
    const isolatedFraction = params.symptomatic_cases_to_be_isolated / 100;

    const caseIsolationCapacity = params.case_isolation_capacity * params.population_size * 100;
    const caseIsolationBegin = params.case_isolation_begin;
    const caseIsolationEnd = caseIsolationBegin + params.case_isolation_range;

    const generalContactReduction = params.contact_reduction_factor / 100;
    const contactReductionBegin = params.contact_reduction_begin;
    const contactReductionEnd = contactReductionBegin + params.contact_reduction_range;

    const sickThreshold = params.sick_threshold * params.population_size * 10000;
    const hospitalisationThreshold = params.hospitalisation_threshold * params.population_size * 10;
    const icuThreshold = params.icu_threshold * params.population_size / 100;

    const sickTriggeredContactReduction = params.sick_triggered_contact_reduction / 100;
    const hospitalisationTriggeredContactReduction = params.hospitalisation_triggered_contact_reduction / 100;
    const icuTriggeredContactReduction = params.icu_triggered_contact_reduction / 100;

    // console.log('Factor', generalContactReduction, sickTriggeredContactReduction);

    // console.log(params);
    // console.log(params.sick_threshold, params.population_size);
    // console.log('Tresholds', sickThreshold, hospitalisationThreshold, icuThreshold);
    // console.log('Factor', sickTriggeredContactReductionFactor,
    //       hospitalisationTriggeredContactReductionFactor, icuTriggeredContactReductionFactor);

    const sum = (a: number, b: number) => a + b;

    let doContactReduction = false;
    let contactReduction = 0;

    let lastCII = 0;
    let lastCIC = 0;
    let lastDead = 0;

    for ( let day = x0; day <= params.simulation_days; day++) {

      if ((day > contactReductionBegin) && (day <= contactReductionEnd)) {
        doContactReduction = true;
        contactReduction = Math.max(contactReduction, generalContactReduction);
      }
      CR.values.push( { x: day, y: contactReduction} );

      dgl.set('contactReduction', doContactReduction ? 1 - contactReduction : 1);

      const doCaseIsolation = (day > caseIsolationBegin) && (day <= caseIsolationEnd);
      dgl.set('isolationCapacity', doCaseIsolation ? caseIsolationCapacity : -1);

      rk4.run(day);
      const y = rk4.getY();

      const susceptible   = population.susceptible(y);
      const latent        = population.latent(y);
      const prodromal     = population.prodromal(y);
      const infective     = population.infective(y);
      const declining     = population.convalescent(y);
      const removed       = population.removed(y);

      // console.log(susceptible, latent, prodromal, infective, convalescent, removed );

      const infected = latent + prodromal + infective + declining;
      const contagious = infective + declining;

      const symptomatic = contagious * symptomaticFraction;
      const asymptomatic = contagious - symptomatic;

      const dead = removed * symptomaticFraction * caseFatality;
      const recovered = removed - dead;

      // const hospitalised = symptomatic * hospitalizedFraction;
      // const icu = hospitalised * icuFractionHospitalized;

      const detected = symptomatic * isolatedFraction;

      const isolated = doCaseIsolation ? Math.min( detected, caseIsolationCapacity) : 0;
      const secluded = doCaseIsolation ? detected - isolated : 0;



      const currentSeasonalFactor = 1 + (params.maximal_seasonal_transmission_factor / 100)
        * Math.cos(SINUSFACTOR * ( day - params.maximal_seasonal_transmission_day ));
      const r0 = params.basic_reproduction_number * currentSeasonalFactor;

      const cii = y[population.offset.CII];
      const cic = y[population.offset.CIC];
      const cis = cic * symptomaticFraction;
      const cia = cic - cis;
      const cim = cis * medicalConsultations;
      const cih = cis * hospitalizedFraction;
      const cix = cih * icuFractionHospitalized;

      const dii = cii - lastCII;
      const dic = cic - lastCIC;
      const dis = dic * symptomaticFraction;
      const dia = dic - dis;
      const dim = dis * medicalConsultations;
      const dih = dis * hospitalizedFraction;
      const dix = dih * icuFractionHospitalized;
      const did = dead - lastDead;

      const hspDur = params.hospitalisation_duration;
      const icuDur = params.icu_duration;

      let hospitalised = dih;
      for (let  t = day - 1; t > Math.max(0, day - Math.floor(hspDur)); t--) {
        hospitalised += DIH.values[t].y;
      }
      if ((day - Math.ceil(hspDur)) >= 0) {
        hospitalised += DIH.values[day - Math.ceil(hspDur)].y * (hspDur - Math.trunc(hspDur));
      }

      let icu = dix;
      for (let  t = day - 1; t > Math.max(0, day - icuDur); t--) {
        icu += DIX.values[t].y;
      }
      if ((day - Math.ceil(icuDur)) >= 0) {
        icu += DIX.values[day - Math.ceil(icuDur)].y * (icuDur - Math.trunc(icuDur));
      }

      lastCII = cii;
      lastCIC = cic;
      lastDead = dead;

      const dm = params.detection_probability_medical_consultation / 100;
      const dh = params.detection_probability_hospitalisation / 100;
      const dx = params.detection_probability_deceased_case / 100;

      const dp = 1 - (1 - dm) ** cim * (1 - dh) ** cih * (1 - dx) ** dead;

      const pop = susceptible + latent + prodromal + infective + declining + recovered + dead;


      doContactReduction = false;
      contactReduction = 0;
      if ( (symptomatic > sickThreshold) && (sickTriggeredContactReduction > 0) ) {
        // console.log('SickTrigger', day);
        doContactReduction = true;
        contactReduction = Math.max(contactReduction, sickTriggeredContactReduction);
      }

      if ( (hospitalised > hospitalisationThreshold)
        && (hospitalisationTriggeredContactReduction > 0 ) ) {

        doContactReduction = true;
        contactReduction = Math.max(contactReduction, hospitalisationTriggeredContactReduction);
        console.log('Hospitalisation Trigger', day, contactReduction);
      }

      if ( (icu > icuThreshold) && (icuTriggeredContactReduction > 0) ) {
        // console.log('ICU Trigger', day);
        doContactReduction = true;
        contactReduction = Math.max(contactReduction, icuTriggeredContactReduction);
      }

      S.values.push( { x: day, y: susceptible} );
      I.values.push( { x: day, y: infected} );
      R.values.push( { x: day, y: recovered} );
      D.values.push( { x: day, y: dead} );
      DP.values.push( { x: day, y: dp} );
      R0.values.push( { x: day, y: r0} );

      L.values.push( { x: day, y: latent} );
      P.values.push( { x: day, y: prodromal} );
      A.values.push( { x: day, y: asymptomatic} );
      Y.values.push( { x: day, y: symptomatic} );
      H.values.push( { x: day, y: hospitalised} );
      X.values.push( { x: day, y: icu} );

      IC.values.push( { x: day, y: isolated} );
      SC.values.push( { x: day, y: secluded} );

      // IC.values.push( { x: day, y: rk4.getEvals()} );
      // HIC.values.push( { x: day, y: rk4.getSteps()} );

      Popsize.values.push( { x: day, y: pop} );

      CII.values.push( { x: day, y: cii} );
      CIS.values.push( { x: day, y: cis} );
      CIM.values.push( { x: day, y: cim} );
      CIH.values.push( { x: day, y: cih} );
      CIX.values.push( { x: day, y: cix} );
      CID.values.push( { x: day, y: dead} );

      DII.values.push( { x: day, y: dii} );
      DIS.values.push( { x: day, y: dis} );
      DIM.values.push( { x: day, y: dim} );
      DIH.values.push( { x: day, y: dih} );
      DIX.values.push( { x: day, y: dix} );
      DID.values.push( { x: day, y: did} );
    }

    const weekly = (values) => {
      const results = [];
      const myValues = [...values];
      while (myValues.length) {
        results.push( { x: (myValues[0].x - 1) / 7, y: myValues.splice(0, 7).map(e => e.y).reduce( sum ) } );
      }
      return results;
    };

    WII.values = weekly(DII.values);
    // WIA.values = weekly(DIA.values);
    WIS.values = weekly(DIS.values);
    WIM.values = weekly(DIM.values);
    WIH.values = weekly(DIH.values);
    WIX.values = weekly(DIX.values);
    WID.values = weekly(DID.values);

    const result = {
      popsize: Popsize,

      susceptible: S,
      infected: I,
      recovered: R,
      dead: D,
      detection: DP,
      contactReduction: CR,

      r0: R0,

      latent: L,
      prodromal: P,
      asymptomatic: A,
      sick: Y,
      hospitalized: H,
      icu: X,
      isolated: IC,
      secluded: SC,

      CIinfections: CII,
      CIsick: CIS,
      CIoutpatients: CIM,
      CIhospitalisations: CIH,
      CIicu: CIX,
      CIdeaths: CID,

      DIinfections: DII,
      DIsick: DIS,
      DIoutpatients: DIM,
      DIhospitalisations: DIH,
      DIicu: DIX,
      DIdeaths: DID,

      WIinfections: WII,
      WIsick: WIS,
      WIoutpatients: WIM,
      WIhospitalisations: WIH,
      WIicu: WIX,
      WIdeaths: WID,

      update: request.update
    };

    if ( loggingEnabled ) {
      console.log('Simulation result', result);
    }
    return result;
  }

}
