import {SEPIRstagesSimulation} from './app/model/simulation';

const { Worker, isMainThread, parentPort, workerData, threadId } = require('worker_threads');

const zmq = require('zeromq');

if (isMainThread) {
    const yargs = require('yargs');

    const argv = yargs
        .option('port', {
            alias: 'p',
            description: 'port to listen to (default: 3000)',
            type: 'number',
            default: 3000
        })
        .option('cores', {
            alias: 'c',
            description: 'number of CPU cores to use (default: all available)',
            type: 'number',
            default: require('os').cpus().length - 2
        })
        .help()
        .alias('help', 'h')
        .argv;

    const port = argv.port ? argv.port : 3000;

    const numCPUs = argv.cores;

    const workers = [];
    for (let i = 0; i < numCPUs; i++) {
        const worker = new Worker(__filename, {workerData: port + i});
        worker.on('exit', (code) => {
            if (code !== 0) {
                console.log(new Error(`Worker stopped with exit code ${code}`));
            }
        });
        workers.concat(worker);
    }
    console.log(`Main worker ${process.pid} running`);
} else {
    console.log(`Starting worker ${threadId}...`);
    simulate_loop();
}

async function simulate_loop() {
    const sock = new zmq.Pair();

    const workerPort = workerData;

    await sock.bind('tcp://*:' + workerPort).catch('unable to bind to port ' + workerPort);
    console.log('worker ' + threadId + ' listening on port ' + workerPort + '...');

    for await (const params of sock) {
        const response = simulate(params);
        await sock.send(response);
    }
}

function simulate(params) {
    // console.log(`processing on Worker ${threadId}`);
    const sim = new SEPIRstagesSimulation();
    const paramsString = String.fromCharCode.apply(null, params[0]);
    const parameters /*: ParameterSettings*/ = JSON.parse(paramsString);
    const request = {parameters, update: -1} /*as SimulationRequest*/;
    const response /*: SimulationResult*/ = sim.simulate(request, false);
    const stringResponse = JSON.stringify(response);
    return stringResponse;
}
