import {SEPIRstagesSimulation} from './app/model/simulation';
import { SimulationRequest } from './app/model/simulation-request';
import { SimulationResult } from './app/model/simulation-result';
import {ParameterSettings} from './app/model/parameter-settings';

const app = require('express')();
const cluster = require('cluster');
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const yargs = require('yargs');

const argv = yargs
    .option('port', {
        alias: 'p',
        description: 'port to listen to (default: 3000)',
        type: 'number',
        default: 3000
    })
    .option('cores', {
        alias: 'c',
        description: 'number of CPU cores to use (default: all available)',
        type: 'number',
        default: require('os').cpus().length - 2
    })
    .help()
    .alias('help', 'h')
    .argv;

const port = argv.port ? argv.port : 3000;

const numCPUs = argv.cores;

// cluster.schedulingPolicy = cluster.SCHED_RR;

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);

    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
    });
} else {
    const simulation = new SEPIRstagesSimulation();

    app.use(bodyParser.json());
    app.post('/', (req, res) => {
        // console.log(`processing on Worker ${process.pid}`);
        const parameters = req.body as ParameterSettings;
        const request = {parameters, update: -1} as SimulationRequest;
        const response: SimulationResult = simulation.simulate(request, false);
        res.status(200).json(response);
    });

    http.listen(port, () => {
        console.log('listening on port ' + port + ', will be using ' + numCPUs + ' CPU cores...');
    });

    console.log(`Worker ${process.pid} started`);
}
