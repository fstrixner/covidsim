# CoronaSim

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.25.

## Preparation

Install the dependencies with `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Running the simulation standalone with node.js

* Run the Typescript compiler with `npx tsc` (make sure `npm install' was executed before)
* Run the bash script `run_local_restapi.sh --port <port>` on Linux or `run_local_restapi.bat --port <port>` on Windows.
The port can be ommitted, by default 3000 is used.
* The simulator is now reachable via POST request to `http://localhost:<port>` that expects a JSON dictionary with parameters
and returns a JSON dictionary with simulated results. The format of the expected and provided dictionaries is
aligned with the specifications in
`src/app/model/parameters-settings.ts` and `src/app/model/simulation-results.ts`
* An example client for Python can be found under `clients/python`
    * install Python (tested with 3.7, should work with any Python 3 version though)
    * optional: create a virtual environment with `python -m venv venv` and activate it with
    `source venv/Scripts/activate` on Linux or `venv/Scripts/activate.bat` on Windows
    * run `pip install -r requirements.txt` to install the dependencies
    * start the client with `python covidsim_python_client.py`

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
